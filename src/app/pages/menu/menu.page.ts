import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  
  dificultad: string  = '';
  alertCtrl: any;
  niveles = ["Fácil", "Normal", "Difícil"]

  
  constructor(private router: Router, public navCtrl: NavController, public alertController: AlertController) {
  }

  toggleTheme(event) {
    if (event.detail.checked) {
      document.body.setAttribute('color-theme', "dark");
    } else {
      document.body.setAttribute('color-theme', 'light');
    }
  }

  gameScreen(){
    let tipoDificultad: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad
      }
    };
    if (this.dificultad) {
      this.router.navigate(['game'], tipoDificultad);
    }else {
     this.alertController.create({
        header: "Dificultad Juego",
        message: "No has escogido el nivel de juego!!!",
        buttons: ['OK']
      }).then(alerta => {
      
        alerta.present();
      
      }); 
    }
  }


 showInstrucciones() {
    this.alertController.create({
      header: 'INSTRUCCIONES',
      message: "1. El jugador ha de adivinar la palabra escondida. <br><br>2. A medida que el jugador continúa, se añade una parte de la imagen del ahorcado, varias letras de la palabra y se muestran los aciertos restantes.<br><br>3. Una vez que la imagen está completa, el juego ha terminado, y el jugador pierde.",
      buttons: ['Cerrar']
    }).then(instrucciones => {
    
      instrucciones.present();
    
    }); 
  }

  
  ngOnInit() {
  }
    
}
