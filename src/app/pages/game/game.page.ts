import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NavController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit {
  
  readonly letras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
  "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

  id: any;

  /*readonly LETRAS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", 
  "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y","Z"];

 readonly arrayFacil = ["abrir", "acido", "actor", "agrio", "agudo", "aguja",
  "anime", "apolo", "aries", "arete", "arido", "atico", "cerdo", "callo",
  "chile", "china", "coral", "volar", "dubai", "emoji", "erizo"];

  readonly arrayNormal = ["aerobic", "aerobus", "aerosol", "agachar", "agalla",
  "babero", "caballa", "caballo", "cabezal", "chabola", "fabrica", "fabula", "faceta", 
  "fachada", "gaccela", "habitat", "iberico", "iconico", "jabalin", "laboral", "laborar",
  "macabro", "macaco", "objeto", "quebrar", "sabado", "tabaco", "tabasco", "taberna",
  "xenismo"];

  readonly arrayDifícil = ["amabilidad", "boardilla", "chocolate", "ebullicion",
  "fabricante", "habichuela", "habitacion", "habitaculo", "habitantes", "idealismo",
  "nacimiento", "nacionales", "obediencia", "pabellones", "rabanillo", "tolerancia",
  "trabajador", "tuberculo", "vacaciones", "xenofobia"];

  niveles = ["Fácil", "Normal", "Difícil"]


  palabraFacil: string;
  palabraAAdivinarFacil: string;

  palabraNormal: string;
  palabraAAdivinarNormal: string;

  palabraDificil: string;
  palabraAAdivinarDificil: string;

  fallos: Array<string>;
  numFallos: number;
  numAciertos: number;
  dificultad: string;

  /*mensajeAcierto: string = "Felicidades!";
  subMensajeAcierto: string = "Has acertado la palabra {{palabra}} en {{numAciertos}}";
  
  mensajeFallo: string = "Fin del Juego!";
  subMensajeFallo: string ="No has adivinado la palabra {{palabra}} en {{numAciertos+numFallos}}";*/

  constructor(public navCtrl: NavController, private route: ActivatedRoute, private router: Router) { 
    this.route.queryParams.subscribe (params => {
      if (params && params.dificultad) {
        this.dificultad = params.dificultad
      }
    })
    this.inicializar();
  }

  
 
  inicializar(): void {
    this.numFallos = 0;
    this.numAciertos = 0;
    this.fallos = [];
    console.log(this.dificultad)
    for (var _x of this.niveles) {
      if (this.dificultad == this.niveles[0]) {
        let numero = Math.floor(Math.random() * this.arrayFacil.length);
        this.palabraAAdivinarFacil = this.arrayFacil[numero];
      } else if (this.dificultad == this.niveles[1]){
        let numero = Math.floor(Math.random() * this.arrayNormal.length);
        this.palabraAAdivinarNormal = this.arrayNormal[numero];
      } else {
        let numero = Math.floor(Math.random() * this.arrayDifícil.length);
        this.palabraAAdivinarDificil = this.arrayDifícil[numero];
      }
    }
    this.palabraRandom();
  }

  palabraRandom(): void {
    this.palabraFacil = "";
    this.palabraNormal = "";
    this.palabraDificil = "";

    for (var _x of this.niveles) {
      if (this.dificultad == this.niveles[0]) {
        for(let i=0; i< this.palabraAAdivinarFacil.length; i++) {
          this.palabraFacil += "_";
        }
      } else if (this.dificultad == this.niveles[1]){
        for(let i=0; i< this.palabraAAdivinarNormal.length; i++) {
          this.palabraNormal += "_";
        }
      } else {
        for(let i=0; i< this.palabraAAdivinarDificil.length; i++) {
          this.palabraDificil += "_";
        }
     }
    }
  }
  

  
  letraAcertada(letra: string): boolean {
    let longitudPalabraF = this.palabraAAdivinarFacil.length;
    let longitudPalabraM= this.palabraAAdivinarNormal.length;
    let longitudPalabraD = this.palabraAAdivinarDificil.length;
    let i =0;
    let letraMinuscula = letra.toLowerCase()
    
    for (var _x of this.niveles) {
      if (this.dificultad == this.niveles[0]) {
      for (let i=0; i<longitudPalabraF; i++) {
        if (letraMinuscula == this.palabraAAdivinarFacil[i]) {
          this.palabraAAdivinarFacil = 
          (i == 0 ? this.palabraAAdivinarFacil : this.palabraAAdivinarFacil.substr(0, i) + letraMinuscula + this.palabraAAdivinarFacil.substring(i + 1));
          var letraAcertada = true;
          console.log(this.palabraAAdivinarFacil)
        } else {
          letraAcertada = false;
        }
      }
    } else if (this.dificultad == this.niveles[0]) {
      for (let i=0; i<longitudPalabraM; i++) {
        if (letraMinuscula == this.palabraAAdivinarNormal[i]) {
          this.palabraAAdivinarNormal = 
          (i == 0 ? this.palabraAAdivinarNormal : this.palabraAAdivinarNormal.substr(0, i) + letraMinuscula + this.palabraAAdivinarNormal.substring(i + 1));
          var letraAcertada = true;
          console.log(this.palabraAAdivinarNormal)
        } else {
          letraAcertada = false;
        }
      }
    } else {
      for (let i=0; i<longitudPalabraD; i++) {
        if (letraMinuscula == this.palabraAAdivinarDificil[i]) {
          this.palabraAAdivinarDificil = 
          (i == 0 ? this.palabraAAdivinarDificil : this.palabraAAdivinarDificil.substr(0, i) + letraMinuscula + this.palabraAAdivinarDificil.substring(i + 1));
          var letraAcertada = true;
          console.log(this.palabraAAdivinarDificil)
        } else {
          letraAcertada = false;
        }
      }
    }
      return letraAcertada;
    }
  }

  letraPulsada(letra: string): void {
    if (this.letraAcertada(letra)) {
      this.numAciertos++;
    } else {
      this.numFallos++;
    }
  }

  disableLeter(letra) {
    letra.srcElement.setAttribute("disabled", true);
  }

  paginaResultados(aciertos, fallos, dificultad): void {
    let datos: NavigationExtras = {
      queryParams: {
        aciertos: this.numAciertos,
        fallos: this.numFallos,
        dificultad: this.dificultad
      }
    };
    this.router.navigate(['result'], datos);
  }

  ngOnInit() {
   
  }
}
