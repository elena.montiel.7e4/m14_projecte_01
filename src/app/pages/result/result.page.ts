import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  dificultad: string;
  numAciertos: number;
  numFallos: number;
  palabraFacil: string;
  palabraNormal: string;
  palabraDificil: string;


  constructor(private router: Router) { }

  playAgain(){
  let navigationsExtras: NavigationExtras = {
    queryParams: {
      dificultad: this.dificultad,
      numAciertos: this.numAciertos,
      numFallos: this.numFallos 
    }
  };
    this.router.navigate(['game'], navigationsExtras);
  }

  ngOnInit() {

  }

}
